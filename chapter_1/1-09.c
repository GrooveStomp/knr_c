#include <stdio.h>

#define MAX_LENGTH 1024

/* Copies input to output, replacing each string of one or more blanks by a
   single blank. */

main()
{
  int length, blanks, counter;
  char c, line[MAX_LENGTH];

  blanks = length = counter = 0;
  c = getchar();

  /* Get input and copy to buffer */
  for (; c != EOF && length < (MAX_LENGTH - 1); c = getchar())
    line[length++] = (char)c;
  line[length] = '\0';

  /* Write copy to output */
  for (; counter < length; counter++)
  {
    blanks = (line[counter] == ' ') ? blanks + 1 : 0;
    if (blanks > 1) continue;
    printf("%c", line[counter]);
  }

  printf("\n");
}
