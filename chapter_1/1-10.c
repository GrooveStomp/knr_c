#include <stdio.h>

#define MAX_LENGTH 1024

/* Copies input to output, replacing each tab with \t, each backspace with \b
   and each backslash with \\. */

main()
{
  int length, blanks, counter;
  char c, line[MAX_LENGTH];

  blanks = length = counter = 0;
  c = getchar();

  /* Get input and copy to buffer */
  for (; c != EOF && length < (MAX_LENGTH - 1); c = getchar())
    line[length++] = (char)c;
  line[length] = '\0';

  /* Write copy to output */
  for (c = line[counter]; counter < length; c = line[++counter])
  {
    if      (c == '\b') printf("\\b");
    else if (c == '\t') printf("\\t");
    else if (c == '\\') printf("\\\\");
    else                printf("%c", c);
  }

  printf("\n");
}
