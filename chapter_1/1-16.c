/* Modification to the sample code preceding this exercise.
   The sample program would only count up to MAXLINE characters, so the goal
   of this program is to count the full length of any arbitrary line, copying
   as much as possible to output. */

#include <stdio.h>
#define MAXLINE 1000 // Maximum input line size

int GetLine(char line[], int maxline);
void Copy(char to[], char from[]);

// Print the longest input line
main()
{
  int len; // Current line length
  int max = 0; // Maximum length seen so far
  char line[MAXLINE]; // Current input line
  char longest[MAXLINE]; // Longest line saved here

  while ((len = GetLine(line, MAXLINE)) > 0)
    if (len > max) {
      max = len;
      Copy(longest, line);
    }

  if (max > 0) printf("%d: %s", len, longest);
  return 0;
}

// GetLine: Read a line into s, return length
int
GetLine(char s[], int Limit)
{
  int c, i, len;

  for (i = 0, len = 0; (c = getchar()) != EOF && c != '\n'; ++len)
    if (i < Limit - 1) s[i++] = c;

  if (c == '\n') {
    s[i] = c;
    ++i;
    ++len;
  }

  s[i] = '\0';
  return len;
}

// Copy: Copy 'From' into 'To'; assume 'To' is big enough
void
Copy(char To[], char From[])
{
  int i = 0;
  while ((To[i] = From[i]) != '\0') ++i;
}
