/* Program that prints its input one word per line. */
#include <stdio.h>

#define MAX_LENGTH 1024

main()
{
  int Length = 0, Blanks = 0, Counter = 0;
  char C, Input[MAX_LENGTH];

  /* Get Input and copy to buffer */
  for (C = getchar(); C != EOF && Length < (MAX_LENGTH - 1); C = getchar())
    Input[Length++] = (char)C;
  Input[Length] = '\0';

  /* Output buffer with each word on its own line */
  for (C = Input[Counter]; Counter < Length; C = Input[++Counter]) {
    Blanks = (C == ' ' || C == '\t' || C == '\n') ? Blanks + 1 : 0;
    if (Blanks > 1) continue;
    else if (C == ' ' || C == '\t' || C == '\n') printf("\n");
    else printf("%c", Input[Counter]);
  }
}
