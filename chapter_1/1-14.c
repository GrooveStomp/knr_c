/* Prints a histogram of the frequencies of different characters in its input. */
#include <stdio.h>
#include <ctype.h>

#define AlphabetSize 26

main()
{
  int C, Counts[AlphabetSize] = { 0 };

  for (C = getchar(); C != EOF; C = getchar()) {
    if (!isalpha(C)) continue;
    Counts[tolower(C) - 'a']++;
  }

  /* Heading for histogram */
  printf("\n");
  printf("Frequencies of word lengths for input:\n");
  printf("======================================\n");

  /* Calculates the largest value stored */
  int HighestCount = 0;
  for (int i = 0; i < AlphabetSize; i++)
    HighestCount = Counts[i] > HighestCount ? Counts[i] : HighestCount;

  /* Prints a histogram with the bars growing vertically */
  for (; HighestCount > 0; --HighestCount) {
    for (int i = 0; i < AlphabetSize; ++i) printf("%c", Counts[i] >= HighestCount ? '#' : ' ');
    printf("\n");
  }
  for (int i = 0; i < AlphabetSize; ++i) printf("-");
  printf("\n");
  for (int i = 0; i < AlphabetSize; ++i) printf("%c", i + 'a');
  printf("\n");
}
