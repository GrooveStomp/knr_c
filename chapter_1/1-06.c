#include <stdio.h>

main()
{
  int c;

  printf("Value of EOF: '%d'\n", EOF);

  for (; c != EOF; c = getchar())
  {
    printf("Value of c = getchar(): '%d'\n", c);
    printf("If all is well, then the value of 'c != EOF' should be one.'\n");
    printf("Value of c != EOF: '%d'\n", c != EOF);
  }

  printf("Outside of for loop\n");
  printf("Value of c = getchar(): '%d'\n", c);
  printf("If all is well, then the value of 'c != EOF' should be zero.\n");
  printf("Value of c != EOF: '%d'\n", c != EOF);
}
