#include <stdio.h>

main()
{
  printf("hello, world\n");
  // warning: unknown escape sequence: '\p'
  // printf("hello, world\p");
  // error: incomplete universal character name \u
  // printf("hello, world\u");
  printf("hello, world\e");
}
