#include <stdio.h>

/* Program to count blanks, tabs and newlines. */
main()
{
  int c, newlines, tabs, blanks;

  newlines = tabs = blanks = 0;
  while ((c = getchar()) != EOF)
  {
    if (c == '\n') ++newlines;
    if (c == '\t') ++tabs;
    if (c == ' ') ++blanks;
  }

  printf("newlines: %d\n", newlines);
  printf("tabs: %d\n", tabs);
  printf("blanks: %d\n", blanks);
}
