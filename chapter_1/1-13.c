/* Prints a histogram of the lengths of words in its input. */
#include <stdio.h>

#define MAX_LENGTH 10
typedef int bool;
#define false 0
#define true 1
#define FIELD_WIDTH 15

main()
{
  int Length = 0, Blanks = 0, Counter = 0, Sizes[MAX_LENGTH] = {0};
  char C, FormattedOut[FIELD_WIDTH];
  bool InWord = false;

  for (C = getchar(); C != EOF; C = getchar()) {
    if (C == ' ' || C == '\t' || C == '\n') {
      if (InWord) {
        int MyLength = Length > MAX_LENGTH - 1 ? MAX_LENGTH-1 : Length;
        Sizes[MyLength]++;
      }

      InWord = false;
      Length = 0;
    }
    else {
      InWord = true;
      Length++;
    }
  }

  /* Heading for histogram */
  printf("\n");
  printf("Frequencies of word lengths for input:\n");
  printf("======================================\n");

  /* Calculates the largest value stored */
  int HighestCount = 0;
  for (int i = 0; i < MAX_LENGTH; i++)
    HighestCount = Sizes[i] > HighestCount ? Sizes[i] : HighestCount;

  /* Prints a histogram with the bars growing vertically */
  for (; HighestCount > 0; --HighestCount) {
    for (int i = 0; i < MAX_LENGTH; ++i) printf("%2c ", Sizes[i] >= HighestCount ? '#' : ' ');
    printf("\n");
  }
  for (int i = 0; i < MAX_LENGTH; ++i) printf("---");
  printf("\n");
  for (int i = 0; i < MAX_LENGTH; ++i) printf("%2d ", i + 1);
  printf("\n");

  /* Prints a histogram with the bars growing horizontally (+X) */
  /*
  for (int i = 0; i < MAX_LENGTH; ++i) {
    sprintf(FormattedOut, "Length %d:", i+1);
    printf("%15s ", FormattedOut);
    for (int j = 0; j < Sizes[i]; ++j) {
      printf("#");
    }
    printf("\n");
  }
  */
}
