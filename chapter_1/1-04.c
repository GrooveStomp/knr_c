#include <stdio.h>

/* print Fahrenheit-Celsius table for fahr = 0, 20, ..., 300; floating-point version */
main()
{
  float fahr, celsius;
  int lower, upper, step;

  lower = 0; /* lower limit of temperature table */
  upper = 300; /* upper limit */
  step = 20; /* step size */

  celsius = lower;
  printf("Celsius Fahrenheit\n");
  while (celsius <= upper) {
    fahr = 32 + (9.0/5.0) * celsius;
    printf("%6.1f %10.0f\n", celsius, fahr);
    celsius = celsius + step;;
  }
}
