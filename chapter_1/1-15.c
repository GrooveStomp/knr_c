/* Rewrite of temperature conversion program from 1.2 to use a function. */
#include <stdio.h>
#include <ctype.h>
#include <string.h>

typedef int bool;
#define true 1
#define false 0

float
FahrenheitToCelsius(int Fahrenheit)
{
  return (5.0/9.0) * (Fahrenheit - 32.0);
}

float
CelsiusToFahrenheit(int Celsius)
{
  return 32.0 + (9.0/5.0) * Celsius;
}


main(int Argc, char *Argv[])
{
  float (*Fn)(int);
  int Lower, Upper, Step, Current;
  bool DefaultToFahrenheit;

  /* Argument parsing and option setup. */
  if (Argc != 2) {
    printf("Usage: ./a.out unit\n");
    printf("\tWhere unit is one of: [fahrenheit, celsius]\n");
    return 0;
  }
  else {
    int C;
    for (int i = 0, c = Argv[1][i]; c != '\0'; c = Argv[1][++i]) {
      Argv[1][i] = isalpha(c) ? tolower(c) : Argv[1][i];
    }
    DefaultToFahrenheit = (strcmp("fahrenheit", Argv[1]) == 0) ? true : false;
  }

  Lower = 0, Upper = 300, Step = 20;
  Current = Lower;

  /* Print out the heading. */
  if (DefaultToFahrenheit)
    printf("Fahrenheit Celsius\n");
  else
    printf("Celsius Fahrenheit\n");

  /* Print out the conversion chart. */
  while (Current <= Upper) {
    Fn = DefaultToFahrenheit ? &FahrenheitToCelsius : &CelsiusToFahrenheit;
    if (DefaultToFahrenheit)
      printf("%10d %7.1f\n", Current, (*Fn)(Current));
    else
      printf("%7d %10.1f\n", Current, (*Fn)(Current));
    Current = Current + Step;
  }
}
